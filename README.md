# Pdfmucker

PDF Mucker is a command-line tool to simulate various types of errors and modifications in PDF files, such as tears, folds, and rotations. This tool can be useful for testing PDF processing applications or for creating examples of damaged PDF files.

## Features

- Tear a corner of a PDF page with a specified severity.
- Fold a corner of a PDF page
- Rotate a PDF page by a specified number of degrees
- Covered QR code
- Scanned page with poor jpeg quality

## Installation

1. Clone the repository or download the script.
2. Install the required Python package:

```bash
pip install PyMuPDF

```

## Usage
```bash
python pdf-mucker.py <filename> <page> <operation> [<corner>] [--severity <severity>]
```
